<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Turbo\TurboBundle;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * CRUD on posts and comments.
 */
class PostController extends AbstractController
{
    /**
     * The number of item on a page for the pagination.
     */
    private const POSTS_PER_PAGE = 10;

    #[Route('/', name: 'app_posts')]
    public function index(PostRepository $repository, Request $request): Response
    {
        $page = $request->query->getInt('page', 1);
        $posts = $repository->findPaginatedPosts($page, self::POSTS_PER_PAGE);
        $maxPage = ceil($posts->count() / self::POSTS_PER_PAGE);
        return $this->render('post/index.html.twig', [
            'posts' => $posts,
            'maxPage' => $maxPage,
            'page' => $page,
        ]);
    }

    #[Route('/posts', name: 'app_post_index', methods: ['GET'])]
    public function table(): Response
    {
        return $this->redirectToRoute('app_posts', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/posts/new', name: 'app_post_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('app_posts', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form,
        ]);
    }

    #[Route('/posts/{id}', name: 'app_post_show', methods: ['GET'])]
    public function show(Post $post): Response
    {
        $form = $this->createForm(CommentType::class, new Comment(), [
            'action' => $this->generateUrl('app_post_comment', ['id' => $post->getId()]),
        ]);
        return $this->render('post/show.html.twig', [
            'post' => $post,
            'form' => $form,
        ]);
    }

    #[Route('/posts/{id}/edit', name: 'app_post_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function edit(Request $request, Post $post, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_posts', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form,
        ]);
    }

    #[Route('/posts/{id}', name: 'app_post_delete', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function delete(Request $request, Post $post, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), (string) $request->getPayload()->get('_token'))) {
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_posts', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/posts/{id}/comment', name: 'app_post_comment', methods: ['POST'])]
    public function publishComment(Request $request, Post $post, EntityManagerInterface $entityManager, #[CurrentUser] User $user): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setRelatedPost($post)
                ->setAuthor($user);
            $entityManager->persist($comment);
            $entityManager->flush();

            if (TurboBundle::STREAM_FORMAT === $request->getPreferredFormat()) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                return $this->renderBlock('comment/new.stream.html.twig', 'success_stream', ['comment' => $comment]);
            }
        }
        return $this->redirectToRoute('app_post_show', ['id' => $post->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/comment/{id}/edit', name: 'app_post_comment_edit', methods: ['GET', 'POST'])]
    public function editComment(Request $request, Comment $comment, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CommentType::class, $comment, [
            'action' => $this->generateUrl('app_post_comment_edit', ['id' => $comment->getId()]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            if ($comment->getRelatedPost() === null) {
                return $this->redirectToRoute('app_posts', [], Response::HTTP_SEE_OTHER);
            }
            return $this->redirectToRoute('app_post_show', [
                'id' => $comment->getRelatedPost()->getId()
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->render('comment/edit.html.twig', [
            'comment' => $comment,
            'form' => $form,
        ]);
    }

    #[Route('/comment/{id}', name: 'app_post_comment_delete', methods: ['POST'])]
    #[IsGranted('COMMENT_EDIT', subject: 'comment')]
    public function deleteComment(Request $request, Comment $comment, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), (string) $request->getPayload()->get('_token'))) {
            $id = $comment->getId();
            $entityManager->remove($comment);
            $entityManager->flush();

            if (TurboBundle::STREAM_FORMAT === $request->getPreferredFormat()) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                return $this->renderBlock('comment/deleted.stream.html.twig', 'success_stream', ['comment' => $id]);
            }
        }

        if ($comment->getRelatedPost() === null) {
            return $this->redirectToRoute('app_posts');
        }
        return $this->redirectToRoute('app_post_show', ['id' => $comment->getRelatedPost()->getId()]);
    }

    #[Route('/post/{id}/like', name: 'app_posts_like', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function addLike(#[CurrentUser] User $user, Post $post, EntityManagerInterface $entityManager): JsonResponse
    {
        $user->addLikedPost($post);
        $entityManager->flush();

        $likesCount = $post->getLikes()->count();

        return new JsonResponse(['success' => true, 'likesCount' => $likesCount]);
    }

    #[Route('/post/{id}/unlike', name: 'app_posts_unlike', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function deleteLike(#[CurrentUser] User $user, Post $post, EntityManagerInterface $entityManager): JsonResponse
    {
        $user->removeLikedPost($post);
        $entityManager->flush();

        $likesCount = $post->getLikes()->count();

        return new JsonResponse(['success' => true, 'likesCount' => $likesCount]);
    }

}
