<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Translation\LocaleSwitcher;

class SettingController extends AbstractController
{
    #[Route('/setting/{locale}', name: 'app_setting_locale')]
    public function index(LocaleSwitcher $localeSwitcher, string $locale, Request $request): Response
    {
        $localeSwitcher->setLocale($locale);
        $request->getSession()->set('_locale', $locale);
        return $this->redirectToRoute('app_posts');
    }
}
