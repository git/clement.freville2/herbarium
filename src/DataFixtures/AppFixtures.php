<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\Species;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Creates fake data for testing purposes.
 */
class AppFixtures extends Fixture
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher
    )
    {
    }

    public function load(ObjectManager $manager): void
    {
        // Dummy user
        $user = (new User())->setEmail('test@test.fr');
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $manager->persist($user);

        // Posts and their species
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 20; ++$i) {
            $name = $faker->name();
            $species = (new Species())
                ->setScientificName($name)
                ->setVernacularName($name)
                ->setRegion($faker->country());
            $date = DateTimeImmutable::createFromMutable($faker->dateTime());
            $post = (new Post())
                ->setFoundDate($date)
                ->setPublicationDate($date)
                ->setLatitude($faker->randomFloat())
                ->setLongitude($faker->randomFloat())
                ->setCommentary($faker->text())
                -> setSpecies($species);
            $manager->persist($species);
            $manager->persist($post);
        }
        $manager->flush();
    }
}
