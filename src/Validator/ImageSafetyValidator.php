<?php

namespace App\Validator;

use App\Service\ImageSafetyServiceInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ImageSafetyValidator extends ConstraintValidator
{
    public function __construct(private readonly ImageSafetyServiceInterface $imageSafetyService)
    {
    }

    /**
     * @param mixed $value
     * @param ImageSafety $constraint
     * @return void
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (null === $value || '' === $value) {
            return;
        }

        if (!$this->imageSafetyService->isValid($value)) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
