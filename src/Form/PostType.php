<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\Species;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('foundDate', null, [
                'widget' => 'single_text',
            ])
            ->add('latitude')
            ->add('longitude')
            ->add('altitude')
            ->add('imageFile', FileType::class, [
                'required' => false,
            ])
            ->add('commentary')
            ->add('species', EntityType::class, [
                'class' => Species::class,
                'choice_label' => 'scientific_name',
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
