<?php

namespace App\Entity;

use ApiPlatform\Metadata;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\SpeciesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SpeciesRepository::class)]
#[ApiResource(
    operations: [new Metadata\Post(), new Metadata\Get(), new Metadata\Put(), new Metadata\Delete(), new Metadata\Patch()],
    normalizationContext: ['groups' => ['species:collection:read', 'species:read']],
)]
#[Metadata\GetCollection(normalizationContext: ['groups' => ['species:collection:read']])]
class Species
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['species:collection:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['species:collection:read'])]
    #[Assert\NotBlank]
    private ?string $scientific_name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['species:collection:read'])]
    #[Assert\NotBlank]
    private ?string $vernacular_name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['species:collection:read'])]
    #[Assert\NotBlank]
    private ?string $region = null;

    /**
     * @var Collection<int, Post>
     */
    #[ORM\OneToMany(targetEntity: Post::class, mappedBy: 'species')]
    #[Groups(['species:read'])]
    private Collection $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScientificName(): ?string
    {
        return $this->scientific_name;
    }

    public function setScientificName(string $scientific_name): static
    {
        $this->scientific_name = $scientific_name;

        return $this;
    }

    public function getVernacularName(): ?string
    {
        return $this->vernacular_name;
    }

    public function setVernacularName(string $vernacular_name): static
    {
        $this->vernacular_name = $vernacular_name;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): static
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): static
    {
        if (!$this->posts->contains($post)) {
            $this->posts->add($post);
            $post->setSpecies($this);
        }

        return $this;
    }

    public function removePost(Post $post): static
    {
        if ($this->posts->removeElement($post)) {
            // set the owning side to null (unless already changed)
            if ($post->getSpecies() === $this) {
                $post->setSpecies(null);
            }
        }

        return $this;
    }
}
