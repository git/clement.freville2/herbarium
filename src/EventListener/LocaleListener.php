<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Translation\LocaleSwitcher;

/**
 * Reads the locale from the user session and change it for every request.
 */
final readonly class LocaleListener
{
    public function __construct(private LocaleSwitcher $localeSwitcher)
    {
    }

    #[AsEventListener(event: KernelEvents::REQUEST)]
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if ($request->attributes->getBoolean('_stateless')) {
            return;
        }
        $locale = $request->getSession()->get('_locale');
        if ($locale !== null) {
            $this->localeSwitcher->setLocale($locale);
        }
    }
}
