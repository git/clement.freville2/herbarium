<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Ensures that an image is safe.
 */
interface ImageSafetyServiceInterface
{
    public function isValid(File $file): bool;
}
