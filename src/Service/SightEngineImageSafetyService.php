<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Contracts\HttpClient\HttpClientInterface;

readonly class SightEngineImageSafetyService implements ImageSafetyServiceInterface
{
    public function __construct(
        private HttpClientInterface $client,
        #[Autowire(env: 'API_USER_SIGHT_ENGINE')] private string $apiUser,
        #[Autowire(env: 'API_KEY_SIGHT_ENGINE')] private string $apiKey,
    )
    {
    }

    public function isValid(File $file): bool
    {
        $handle = fopen($file->getRealPath(), 'r');
        if ($handle === false) {
            return false;
        }
        $response = $this->client->request('POST', 'https://api.sightengine.com/1.0/check.json', [
            'body' => [
                'media' => $handle,
                'models' => 'nudity-2.1',
                'api_user' => $this->apiUser,
                'api_secret' => $this->apiKey,
            ],
        ]);
        fclose($handle);

        $output = $response->toArray();
        $scoreNudity = $output['nudity'];

        return $scoreNudity['sexual_activity'] < 0.8 &&
            $scoreNudity['sexual_display'] < 0.8 &&
            $scoreNudity['erotica'] < 0.8;
    }
}
