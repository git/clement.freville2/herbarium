<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

class DummyImageSafetyService implements ImageSafetyServiceInterface
{
    public function isValid(File $file): bool
    {
        return true;
    }
}
