FROM docker.io/dunglas/frankenphp:1-php8.3

RUN install-php-extensions \
		@composer \
		apcu \
		intl \
		opcache \
		zip \
	;

ENV APP_ENV=prod
ENV FRANKENPHP_CONFIG="import worker.Caddyfile"
ENV COMPOSER_ALLOW_SUPERUSER=1

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY frankenphp/conf.d/app.ini frankenphp/conf.d/app.prod.ini $PHP_INI_DIR/conf.d/
COPY frankenphp/Caddyfile /etc/caddy/Caddyfile
COPY frankenphp/worker.Caddyfile /etc/caddy/worker.Caddyfile
COPY frankenphp/start.sh .

COPY composer.json composer.lock symfony.lock ./
RUN composer install --no-cache --no-dev --no-autoloader --no-scripts --no-progress

COPY . .
RUN composer dump-autoload --classmap-authoritative --no-dev \
    && composer dump-env prod \
    && composer run-script --no-dev post-install-cmd \
    && php bin/console asset-map:compile \
  ;
RUN rm -Rf frankenphp/

ENTRYPOINT [ "/app/start.sh" ]
