<?php

namespace App\Test\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PostControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private PostRepository $repository;
    private string $path = '/posts/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        /** @var EntityManagerInterface $manager */
        $manager = static::getContainer()->get(EntityManagerInterface::class);
        $this->manager = $manager;
        $this->repository = $this->manager->getRepository(Post::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();

        $userRepository = $this->manager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneByEmail('test@test.fr');
        $this->client->loginUser($user);
        $this->client->request('GET', sprintf('%snew', $this->path));
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', '/');

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Posts');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'post[foundDate]' => '2024-01-01 00:00:00',
            'post[latitude]' => '45.0',
            'post[longitude]' => '45.0',
            'post[altitude]' => '500.0',
            'post[commentary]' => 'Testing',
        ]);

        self::assertResponseRedirects('/');

        self::assertSame(1, $this->repository->count());
    }

    public function testShow(): void
    {
        $fixture = new Post();
        $fixture->setFoundDate(new \DateTimeImmutable('2024-01-01 00:00:00'));
        $fixture->setCommentary('Cool stuff');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertSelectorTextContains('h1', 'Post');
    }

    public function testEdit(): void
    {
        $fixture = new Post();
        $fixture->setFoundDate(new \DateTimeImmutable('2024-01-01 00:00:00'));
        $fixture->setCommentary('Cool stuff');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'post[foundDate]' => '2024-03-25 00:00:00',
            'post[latitude]' => '90',
            'post[longitude]' => '90',
            'post[altitude]' => '200',
            'post[commentary]' => 'Something New',
        ]);

        self::assertResponseRedirects('/');

        $fixture = $this->repository->findAll();

        self::assertEquals(new \DateTimeImmutable('2024-03-25 00:00:00'), $fixture[0]->getFoundDate());
        self::assertSame(90., $fixture[0]->getLatitude());
        self::assertSame(90., $fixture[0]->getLongitude());
        self::assertSame(200., $fixture[0]->getAltitude());
        self::assertSame('Something New', $fixture[0]->getCommentary());
    }

    public function testRemove(): void
    {
        $fixture = new Post();
        $fixture->setFoundDate(new \DateTimeImmutable('2024-01-01 00:00:00'));
        $fixture->setCommentary('Cool stuff');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/');
        self::assertSame(0, $this->repository->count());
    }

    public function testPostComment()
    {
        $fixture = new Post();
        $fixture->setFoundDate(new \DateTimeImmutable('2024-01-01 00:00:00'));
        $fixture->setCommentary('Cool stuff');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Comment', [
            'comment[content]' => 'This is a comment',
        ]);

        self::assertResponseRedirects(sprintf('%s%s', $this->path, $fixture->getId()));
        $comments = $this->repository->find($fixture->getId())->getComments();
        self::assertSame(1, $comments->count());
    }

    public function testUploadImage()
    {
        $fixture = new Post();
        $fixture->setFoundDate(new \DateTimeImmutable('2024-01-01 00:00:00'));
        $fixture->setCommentary('Cool stuff');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $file = new UploadedFile(__DIR__ . '/../image.png', 'image.png');

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'post[imageFile]' => $file,
        ]);

        self::assertNotNull($this->repository->find($fixture->getId())->getImage());
    }
}
