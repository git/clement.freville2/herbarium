<?php

namespace App\Test\Controller;

use App\Entity\Species;
use App\Entity\User;
use App\Repository\SpeciesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SpeciesControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private SpeciesRepository $repository;
    private string $path = '/species/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        /** @var EntityManagerInterface $manager */
        $manager = static::getContainer()->get(EntityManagerInterface::class);
        $this->manager = $manager;
        $this->repository = $this->manager->getRepository(Species::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();

        $userRepository = $this->manager->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneByEmail('test@test.fr');
        $this->client->loginUser($user);
        $this->client->request('GET', sprintf('%snew', $this->path));
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Species');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'species[scientific_name]' => 'Testing',
            'species[vernacular_name]' => 'Testing',
            'species[region]' => 'Testing',
        ]);

        self::assertResponseRedirects($this->path);

        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Species();
        $fixture->setScientificName('My Title');
        $fixture->setVernacularName('My Title');
        $fixture->setRegion('My Title');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Species');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $fixture = new Species();
        $fixture->setScientificName('Value');
        $fixture->setVernacularName('Value');
        $fixture->setRegion('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'species[scientific_name]' => 'Something New',
            'species[vernacular_name]' => 'Something New',
            'species[region]' => 'Something New',
        ]);

        self::assertResponseRedirects('/species/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getScientificName());
        self::assertSame('Something New', $fixture[0]->getVernacularName());
        self::assertSame('Something New', $fixture[0]->getRegion());
    }

    public function testRemove(): void
    {
        $fixture = new Species();
        $fixture->setScientificName('Value');
        $fixture->setVernacularName('Value');
        $fixture->setRegion('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/species/');
        self::assertSame(0, $this->repository->count([]));
    }
}
