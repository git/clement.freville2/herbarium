<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class UserApiTest extends ApiTestCase
{
    public function testInsertUser(): void
    {
        $response = static::createClient()->request('POST', '/api/users', [
            'json' => [
                'email' => 'test@test.com',
                'plainPassword' => 'password',
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHasHeader('Content-Location');

        $location = $response->getHeaders()['content-location'][0];
        static::createClient()->request('GET', $location);
        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals([
            '@context' => '/api/contexts/User',
            '@id' => $location,
            '@type' => 'User',
            'email' => 'test@test.com',
        ]);
    }
}
