<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Post;
use App\Entity\Species;

class PostApiTest extends ApiTestCase
{
    private int $speciesId;
    private int $postId;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $manager = $kernel->getContainer()->get('doctrine')->getManager();
        $species = (new Species())
            ->setVernacularName('Oak')
            ->setScientificName('Quercus')
            ->setRegion('Europe');
        $post = (new Post())
            ->setPublicationDate(new \DateTimeImmutable('2024-06-07'))
            ->setFoundDate(new \DateTimeImmutable('2024-06-06'))
            ->setCommentary("maple")
            ->setSpecies($species);
        $manager->persist($species);
        $manager->persist($post);
        $manager->flush();
        $this->speciesId = $species->getId();
        $this->postId = $post->getId();
    }

    public function testFindInSpecies(): void
    {
        $response = static::createClient()->request('GET', '/api/species/' . $this->speciesId);

        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals([
            '@context' => '/api/contexts/Species',
            '@id' => '/api/species/' . $this->speciesId,
            '@type' => 'Species',
            'id' => $this->speciesId,
            'vernacular_name' => 'Oak',
            'scientific_name' => 'Quercus',
            'region' => 'Europe',
            'posts' => ['/api/posts/' . $this->postId],
        ]);
    }

    public function testGetExisting(): void
    {
        $response = static::createClient()->request('GET', '/api/posts/' . $this->postId);

        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals([
            '@context' => '/api/contexts/Post',
            '@id' => '/api/posts/' . $this->postId,
            '@type' => 'Post',
            'id' => $this->postId,
            'foundDate' => '2024-06-06T00:00:00+00:00',
            'publicationDate' => '2024-06-07T00:00:00+00:00',
            'commentary' => 'maple',
            'species' => '/api/species/' . $this->speciesId,
        ]);
    }

    public function testFilterBySpecies(): void
    {
        $response = static::createClient()->request('GET', '/api/posts', [
            'query' => [
                'species' => $this->speciesId,
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            '@context' => '/api/contexts/Post',
            '@id' => '/api/posts',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 1,
            'hydra:member' => [
                [
                    '@type' => 'Post',
                    'id' => $this->postId,
                    'foundDate' => '2024-06-06T00:00:00+00:00',
                    'species' => '/api/species/' . $this->speciesId,
                ],
            ],
        ]);
    }

    public function testPostSetPublicationDate(): void
    {
        $response = static::createClient()->request('POST', '/api/posts', [
            'json' => [
                'foundDate' => '2024-06-06',
                'publicationDate' => '2024-06-07',
                'commentary' => 'maple',
                'species' => '/api/species/' . $this->speciesId,
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            '@context' => '/api/contexts/Post',
            '@type' => 'Post',
            'foundDate' => '2024-06-06T00:00:00+00:00',
            'commentary' => 'maple',
            'species' => '/api/species/' . $this->speciesId,
        ]);
        $this->assertArrayHasKey('publicationDate', $response->toArray(false));
    }
}
