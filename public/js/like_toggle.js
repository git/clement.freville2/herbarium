document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('.like-toggle').forEach(button => {
        button.addEventListener('click', function (event) {
            event.preventDefault();

            let isLiked = this.classList.contains('liked');
            let url = isLiked ? this.dataset.unlikeUrl : this.dataset.likeUrl;

            fetch(url, { method: 'POST' })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        let likesCountElement = this.parentElement.querySelector('.likes-count');
                        likesCountElement.textContent = data.likesCount;
                        this.classList.toggle('liked');
                        this.classList.toggle('not-liked');
                        this.innerHTML = isLiked ? '♡' : '❤️';
                    } else {
                        console.error('Erreur lors du traitement du like/unlike.');
                    }
                })
                .catch(error => {
                    console.error('Erreur lors de la requête fetch:', error);
                });
        });
    });
});
