#!/bin/sh

php bin/console doctrine:migrations:migrate --no-interaction
exec frankenphp run --config /etc/caddy/Caddyfile
